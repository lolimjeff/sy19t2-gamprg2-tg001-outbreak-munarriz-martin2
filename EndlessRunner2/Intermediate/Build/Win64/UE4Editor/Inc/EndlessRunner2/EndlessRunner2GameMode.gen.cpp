// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EndlessRunner2/EndlessRunner2GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEndlessRunner2GameMode() {}
// Cross Module References
	ENDLESSRUNNER2_API UClass* Z_Construct_UClass_AEndlessRunner2GameMode_NoRegister();
	ENDLESSRUNNER2_API UClass* Z_Construct_UClass_AEndlessRunner2GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_EndlessRunner2();
// End Cross Module References
	void AEndlessRunner2GameMode::StaticRegisterNativesAEndlessRunner2GameMode()
	{
	}
	UClass* Z_Construct_UClass_AEndlessRunner2GameMode_NoRegister()
	{
		return AEndlessRunner2GameMode::StaticClass();
	}
	struct Z_Construct_UClass_AEndlessRunner2GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEndlessRunner2GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EndlessRunner2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEndlessRunner2GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "EndlessRunner2GameMode.h" },
		{ "ModuleRelativePath", "EndlessRunner2GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEndlessRunner2GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEndlessRunner2GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEndlessRunner2GameMode_Statics::ClassParams = {
		&AEndlessRunner2GameMode::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008802A8u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AEndlessRunner2GameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AEndlessRunner2GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEndlessRunner2GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEndlessRunner2GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEndlessRunner2GameMode, 4036006168);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEndlessRunner2GameMode(Z_Construct_UClass_AEndlessRunner2GameMode, &AEndlessRunner2GameMode::StaticClass, TEXT("/Script/EndlessRunner2"), TEXT("AEndlessRunner2GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEndlessRunner2GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
