// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNER2_EndlessRunner2Character_generated_h
#error "EndlessRunner2Character.generated.h already included, missing '#pragma once' in EndlessRunner2Character.h"
#endif
#define ENDLESSRUNNER2_EndlessRunner2Character_generated_h

#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_RPC_WRAPPERS
#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunner2Character(); \
	friend struct Z_Construct_UClass_AEndlessRunner2Character_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunner2Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlessRunner2"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunner2Character)


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunner2Character(); \
	friend struct Z_Construct_UClass_AEndlessRunner2Character_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunner2Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlessRunner2"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunner2Character)


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEndlessRunner2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunner2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunner2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunner2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunner2Character(AEndlessRunner2Character&&); \
	NO_API AEndlessRunner2Character(const AEndlessRunner2Character&); \
public:


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunner2Character(AEndlessRunner2Character&&); \
	NO_API AEndlessRunner2Character(const AEndlessRunner2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunner2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunner2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEndlessRunner2Character)


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AEndlessRunner2Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AEndlessRunner2Character, FollowCamera); }


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_9_PROLOG
#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_RPC_WRAPPERS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_INCLASS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner2_Source_EndlessRunner2_EndlessRunner2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
