// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacter.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/FloatingPawnMovement.h" // Search the class in Unreal API to get the exact directory of the header file
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use the constructor to create components for this actor

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	SetRootComponent(SkeletalMesh);

	// Add floating pawn movement component
	Movement = CreateDefaultSubobject<UFloatingPawnMovement>("Movement");

	// Setup camera arm
	CameraArm = CreateDefaultSubobject<USpringArmComponent>("CameraArm");
	CameraArm->SetupAttachment(SkeletalMesh);
	// Initial length (can be modified in the blueprint)
	CameraArm->TargetArmLength = 500.0f;

	// Setup camera
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	// Attach the camera to the spring arm. This allows the camera to move with the spring arm
	Camera->SetupAttachment(CameraArm);
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	// Set a random scale for the static mesh when the game starts
	SkeletalMesh->SetWorldScale3D(FMath::VRand());
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

