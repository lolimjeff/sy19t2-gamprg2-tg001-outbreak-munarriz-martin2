// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNER2_EndlessRunner2GameMode_generated_h
#error "EndlessRunner2GameMode.generated.h already included, missing '#pragma once' in EndlessRunner2GameMode.h"
#endif
#define ENDLESSRUNNER2_EndlessRunner2GameMode_generated_h

#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_RPC_WRAPPERS
#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunner2GameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunner2GameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunner2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/EndlessRunner2"), ENDLESSRUNNER2_API) \
	DECLARE_SERIALIZER(AEndlessRunner2GameMode)


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunner2GameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunner2GameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunner2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/EndlessRunner2"), ENDLESSRUNNER2_API) \
	DECLARE_SERIALIZER(AEndlessRunner2GameMode)


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENDLESSRUNNER2_API AEndlessRunner2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunner2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNER2_API, AEndlessRunner2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunner2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNER2_API AEndlessRunner2GameMode(AEndlessRunner2GameMode&&); \
	ENDLESSRUNNER2_API AEndlessRunner2GameMode(const AEndlessRunner2GameMode&); \
public:


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNER2_API AEndlessRunner2GameMode(AEndlessRunner2GameMode&&); \
	ENDLESSRUNNER2_API AEndlessRunner2GameMode(const AEndlessRunner2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNER2_API, AEndlessRunner2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunner2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEndlessRunner2GameMode)


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_9_PROLOG
#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_RPC_WRAPPERS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_INCLASS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner2_Source_EndlessRunner2_EndlessRunner2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
