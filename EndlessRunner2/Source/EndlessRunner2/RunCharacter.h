// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

UCLASS()
class ENDLESSRUNNER2_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Makes the static mesh accessible in the editor
	UPROPERTY(EditAnywhere, Category = "Components")
	class UFloatingPawnMovement* Movement;

	// Adding your own camera component allows you to override the default pawn camera. We want the camera to be behind of our pawn
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UCameraComponent* Camera;

	// Camera can be attached to this so that when it detects collision it will adjust the camera's position.
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USpringArmComponent* CameraArm;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USkeletalMeshComponent* SkeletalMesh;

	// Defines the bullet spawn distance from the actor
	// BlueprintReadOnly gives access to the blueprint nodes.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shooting")
	float BulletSpawnOffset;

	// Input bindings
	void MoveForward(float scale);
	void MoveRight(float scale);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
